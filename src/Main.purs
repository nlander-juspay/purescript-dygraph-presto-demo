module Main where

import Prelude (Unit, bind, discard, pure, unit, ($), (<>), (*>), (<<<), (>>=))
import Bindings.Dygraph (queryDocElt, newDygraph)
import Components.Button as Button
import Control.Monad.State.Trans as S
import Data.Either (Either(..))
import Data.Function.Uncurried (runFn2)
import Data.Maybe (fromMaybe)
import Data.String (splitAt)
import Foreign.Object (empty)
import Effect (Effect)
import Effect.Aff (Aff, launchAff_, makeAff, nonCanceler)
import Effect.Aff.AVar (new)
import Effect.Class (liftEffect)
import Effect.Class.Console (log)
import Engineering.Helpers.Commons (callAPI', mkNativeRequest, showUI')
import Engineering.OS.Permission (checkIfPermissionsGranted, requestPermissions)
import PrestoDOM.Types.Core (Screen)
import Unsafe.Coerce (unsafeCoerce)
import Web.DOM.ParentNode (QuerySelector(..))
import Presto.Core.Flow (APIRunner, Flow, PermissionCheckRunner, PermissionRunner(..), PermissionTakeRunner, Runtime(..), UIRunner, run, initUI, runScreen)

{-
main :: Effect Unit
main = do
  _ <- launchAff_ do
    _ <- makeAff (\cb -> initUI cb)
    _ <- runUI Button.screen "2" $ do
      let gidRec = splitAt 1 Button.graphId
          escapedId = "#\\3" <> gidRec.before <> " " <> gidRec.after
      log escapedId
      mElt <- queryDocElt $ QuerySelector escapedId
      let csv = "Date,Temperature\n" <> "2008-05-07,75\n" <> "2008-05-08,70\n" <> "2008-05-09,80\n"
          elt = fromMaybe (unsafeCoerce unit) mElt
      log csv
      graph <- newDygraph elt csv
      pure unit
    pure unit
  pure unit

runUI :: forall a r s. Screen a s r -> String -> Effect Unit -> Aff Unit
runUI screen  txt eff = do
  _ <- makeAff (\cb -> do 
                  canceler <- runScreen screen cb
                  _ <- eff
                  pure canceler)
  log $ "Completed " <> txt
-}

main :: Effect Unit
main = do
  let runtime = Runtime uiRunner permissionRunner apiRunner
  let freeFlow = S.evalStateT (run runtime appFlow)
  launchAff_ (new empty >>= freeFlow)
  where
    uiRunner :: UIRunner
    uiRunner a = makeAff (\cb -> runFn2 showUI' (cb <<< Right) a *> pure nonCanceler)

    permissionCheckRunner :: PermissionCheckRunner
    permissionCheckRunner = checkIfPermissionsGranted

    permissionTakeRunner :: PermissionTakeRunner
    permissionTakeRunner = requestPermissions

    permissionRunner :: PermissionRunner
    permissionRunner = PermissionRunner permissionCheckRunner permissionTakeRunner

    apiRunner :: APIRunner
    apiRunner request = makeAff (\cb ->
                                  callAPI'
                                    (cb <<< Left)
                                    (cb <<< Right)
                                    (mkNativeRequest request)
                                  *> pure nonCanceler
                                )



appFlow :: Flow Unit
appFlow = do
  _ <- initUI
  runScreen Button.screen

{-
loginFlow :: Flow Unit
loginFlow = do
  result <- runScreen LoginForm.screen
  case result of
       LoginSuccess user -> do
           _ <- runScreen (LoginSuccess.screen user)
           pure unit
       LoginFailed ->
           loginFlow
  pure unit
-}
