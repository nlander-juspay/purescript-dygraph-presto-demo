module Components.LoginForm where

import Prelude
import Effect (Effect)
import PrestoDOM.Elements.Elements
import PrestoDOM.Properties
import PrestoDOM.Types.DomAttributes
import PrestoDOM.Events (onChange, onClick)
import PrestoDOM.Types.Core (PrestoDOM, Screen)
import PrestoDOM.Core (mapDom)
import PrestoDOM (exit, updateAndExit, continue, Eval)
import Components.FormField as FormField

data Action =
    UsernameAction FormField.Action
  | PasswordAction FormField.Action
  | SubmitClicked

type State =
  { errorMessage :: String
  , usernameState :: FormField.State
  , passwordState :: FormField.State
  , visibility :: Visibility
  }

initialState :: State
initialState =
  { errorMessage : ""
  , usernameState : (FormField.initialState "username")
  , passwordState : (FormField.initialState "password")
  , visibility : VISIBLE
  }

eval :: forall eff. Action -> State -> Eval Action Unit State
eval (UsernameAction action) state = continue $ state { usernameState = FormField.eval action state.usernameState }
eval (PasswordAction action) state = continue $ state { passwordState = FormField.eval action state.passwordState }
eval SubmitClicked state =
    if state.passwordState.value == "blueberry" && state.usernameState.value /= ""
        then (updateAndExit (state { visibility = GONE }) unit)
        else (continue $ state { errorMessage = "Your account is blocked" })

screen :: forall eff. Screen Action State Unit
screen =
  { initialState
  , view
  , eval
  }

view :: forall w eff. (Action ->  Effect Unit) -> State -> PrestoDOM (Effect Unit) w
view push state =
  linearLayout
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , background "#323232"
    , gravity CENTER
    ]
    [ linearLayout
      [ height $ V 600
      , width $ V 400
      , background "#f0f0f0"
      , orientation VERTICAL
      , visibility state.visibility
      , gravity CENTER
      ]
      [ (mapDom FormField.view push state.usernameState UsernameAction [])
      , (mapDom FormField.view push state.passwordState PasswordAction [])
      , linearLayout
        [ height $ V 150
        , width MATCH_PARENT
        , orientation VERTICAL
        , margin $ Margin 20 20 20 20
        , gravity CENTER
        ]
        [ textView
          [ height $ V 50
          , width MATCH_PARENT
          , margin $ MarginHorizontal 20 20
          , text state.errorMessage
          ]
        , linearLayout
          [ height $ V 50
          , width MATCH_PARENT
          , margin $ Margin 20 20 20 20
          , background "#969696"
          , gravity CENTER
          , visibility VISIBLE
          , onClick push (const SubmitClicked)
          ]
          [
            textView
            [ width (V 80)
            , height (V 25)
            , text "Submit"
            , textSize 28
            ]
          ]
        ]
      ]
    ]
