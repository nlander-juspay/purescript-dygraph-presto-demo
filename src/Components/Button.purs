module Components.Button where

import Prelude
import Effect (Effect)
import Halogen.VDom.Thunk (Thunk)
import PrestoDOM.Utils (continue)
import PrestoDOM.Elements.Elements (viewWidget, webView, textView, button, linearLayout, linearLayout_)
import PrestoDOM.Types.DomAttributes ( Margin(Margin, MarginHorizontal)
                                     , Orientation(VERTICAL)
                                     , Length(V, MATCH_PARENT)
                                     , Visibility(VISIBLE)
                                     , Gravity(CENTER) )
import PrestoDOM.Events (onClick)
import PrestoDOM.Properties ( margin
                            , width
                            , height
                            , text
                            , textSize
                            , background
                            , color
                            , gravity
                            , id
                            , visibility
                            , orientation )
import PrestoDOM.Types.Core (Namespace(..), VDom, Prop, Screen, Eval, PrestoDOM)
import Web.DOM.Node (Node)

import Bindings.Dygraph (exampleDygraph)

data Action = Click

type State =
  { count :: Int
  , csv :: String
  }

initialState :: State
initialState =
  { count: 0
  , csv: "Date,Temperature\n" <> "2008-05-01,75\n" <> "2008-05-02,70\n" <> "2008-05-03,80\n"
  }

eval :: Action -> State -> Eval Action Unit State
eval Click state = continue (state { count = newCount, csv = state.csv <> "2008-05-0" <> show (newCount + 3) <> "," <> show (80 + 5 * newCount) <> "\n" })
  where
    newCount = state.count + 1

screen :: forall eff. Screen Action State Unit
screen =
  { initialState
  , view
  , eval
  }

graphId :: String
graphId = "123123"

view :: (Action -> Effect Unit)
     -> State
     -> PrestoDOM (Effect Unit) (Thunk Effect Node)
view push state =
  linearLayout_ (Namespace "counter")
    [ height MATCH_PARENT
    , width MATCH_PARENT
    , background "#111111"
    , gravity CENTER
    ]
    [ linearLayout
      [ height $ V 700
      , width $ V 800
      , background "#000000"
      , orientation VERTICAL
      , gravity CENTER
      ]
      [ linearLayout
          [ height $ V 10
          , width $ V 20
          , margin $ Margin 20 0 5 10
          ]
          []
      , linearLayout
        [ width MATCH_PARENT
        , height MATCH_PARENT
        , background "#c3cee0"
        ]
        [ exampleDygraph state.csv
        ]
      , linearLayout
        [ height $ V 150
        , width MATCH_PARENT
        , orientation VERTICAL
        , margin $ Margin 20 20 20 20
        , gravity CENTER
        ]
        [ linearLayout
          [ height $ V 50
          , width MATCH_PARENT
          , margin $ MarginHorizontal 20 20
          ]
          [
            textView
            [ width MATCH_PARENT
            , text $ show state.count
            , textSize 50
            , color "#ffffff"
            , gravity CENTER
            ]
          ]
        , linearLayout
          [ height $ V 50
          , width MATCH_PARENT
          , margin $ Margin 20 50 20 20
          , background "#969696"
          , gravity CENTER
          ]
          [
            textView
            [ width (V 150)
            , height (V 25)
            , text "COUNT"
            , textSize 28
            , gravity CENTER
            , onClick push (const Click)
            ]
          ]
        ]
      ]
    ]
