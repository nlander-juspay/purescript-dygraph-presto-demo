"use strict"

exports.newDygraph = function(elt) {
  console.log(elt);
  return function(csv) {
    return function() {
      return new Dygraph( elt, csv );
    }
  }
}

exports.resize = function(graph) {
  return function(width) {
    return function(height) {
      return function() {
        graph.resize(width, height);
      }
    }
  }
}
