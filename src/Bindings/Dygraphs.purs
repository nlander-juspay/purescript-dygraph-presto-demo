module Bindings.Dygraph
  ( queryDocElt
  , newDygraph
  , resize
  , exampleDygraph
  , Dygraph
  ) where

import Prelude (Unit, bind, ($), unit, pure, discard, (<>), show, (==))
import Data.Function.Uncurried (runFn2, runFn4)
import Data.Maybe (Maybe, fromMaybe)
import Data.String (splitAt)
import Halogen.VDom (VDom(Widget))
import Halogen.VDom.Thunk (Thunk, thunk1)
import Web.HTML (window)
import Web.HTML.HTMLDocument (toParentNode)
import Web.HTML.Window (document)
import Web.DOM.ParentNode (QuerySelector(..), querySelector)
import Web.DOM.Internal.Types (Element)
import Web.DOM.Node (Node)
import Effect (Effect)
import Effect.Console (log)
import Effect.Uncurried (EffectFn1, mkEffectFn1, runEffectFn1)
import PrestoDOM.Core (renderWidget)
import PrestoDOM.Types.Core (PrestoDOM)
import Unsafe.Coerce (unsafeCoerce)
-- import Control.Monad.Eff (Eff, kind Effect)

-- foreign import data RunDygraph :: Effect

foreign import data Dygraph :: Type

foreign import newDygraph :: Element -> String -> Effect Dygraph

foreign import resize :: Dygraph -> Int -> Int -> Effect Unit

queryDocElt :: QuerySelector -> Effect (Maybe Element)
queryDocElt selector = do
  win <- window
  doc <- document win
  elt <- querySelector selector (toParentNode doc)
  pure $ elt

dygraphWidget :: String -> Int -> Effect Unit
dygraphWidget csv parentId = do
  let gidRec = splitAt 1 $ show parentId
      escapedId = "#\\3" <> gidRec.before <> " " <> gidRec.after
  log escapedId
  mElt <- queryDocElt $ QuerySelector escapedId
  let elt = fromMaybe (unsafeCoerce unit) mElt
  graph <- newDygraph elt csv
  pure unit

renderDygraph :: String -> Effect Node
renderDygraph csv = do
  log csv
  w <- runEffectFn1 renderWidget $ dygraphWidget csv
  pure $ unsafeCoerce w

exampleDygraph :: String -> PrestoDOM (Effect Unit) (Thunk Effect Node)
exampleDygraph csv = Widget $ runFn2 thunk1 renderDygraph csv
